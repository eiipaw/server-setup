#!/bin/bash
#
# Bash script for generating new subdomain with a new server block in NGINX
clear

# Functions
ok() { echo -e '\e[32m'$1'\e[m'; } # Green
die() { echo -e '\e[1;31m'$1'\e[m'; exit 1; }

# Check if this script has been run as root
if [ "$EUID" -ne 0 ]
  then 
    die "Script must be running as root."
fi

# Variable definitions
ok "Initializing variables"
NGINX_AVAILABLE_VHOSTS='/etc/nginx/sites-available'
NGINX_ENABLED_VHOSTS='/etc/nginx/sites-enabled'
WEB_DIR='/var/www'
NGINX_SCHEME='$scheme'
NGINX_REQUEST_URI='$request_uri'
# Initialize empty variable for phpmyadmin part, servername and certbot domain
PHPMYADMIN_BLOCK=""
SERVERNAME=""
CERTBOT_DOMAIN=""

USAGE="$(basename "$0") -f -d -e -t [-s -w]
\n\n
***NGINX subdomain server block generator***
\n\n
where:\n
    -t  Type of installation TYPE=string  VALUES=prod|local DEFAULT=N/A REQUIRED=yes\n
    -f  Folder that will be created in \$WEB_DIR TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=yes\n
    -d  Domain name to use in NGINX block  TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=yes\n
    -e  Email to use with certbot TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=yes(if prod)\n
    -s  Subdomain name to use in NGINX block TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=no\n
    -w  \$WEB_DIR Directory for application(must be absolute path) TYPE=string  VALUES=N/A DEFAULT=/var/www REQUIRED=no"



#Assign arguments to variables
 while getopts f:d:s:e:t:w:h: option
    do
    case "${option}" in
        f) FOLDER=${OPTARG};;
        d) DOMAIN=${OPTARG};;
        s) SUBDOMAIN=${OPTARG};;
        e) EMAIL=${OPTARG};;
        t) TYPE=${OPTARG};;
        w) WEB_DIR=${OPTARG};;
        h) echo "$USAGE" exit;;
    esac
 done 

#Check if all the required variables are set
if [[ -z "$FOLDER" || -z "$DOMAIN" || -z "$TYPE" ]]
 then
    die "$USAGE"
fi

#Check if the required variables are set properly
if [[ $TYPE != "prod" && $TYPE != "local" ]]
 then
    die "$USAGE"
fi

if [[ $TYPE == "prod" && -z "$EMAIL" ]]
then
    die "$USAGE"
fi

#
# START NGINX SERVER BLOCK SETUP
#

# Check if our folder name is base or html
if [[ $FOLDER == "base" || $FOLDER == "html" ]] && [ $TYPE == "prod" ] ;then

# Add block for phpmyadmin route
ok "Setting up special block for phpmyadmin"
PHPMYADMIN_BLOCK='location /pmasecure {
                auth_basic "Admin Login";
                auth_basic_user_file /etc/nginx/pma_pass;
          }'

# Set server name to domain name and domain name with www. prefix
ok "Setting up server names and certbot domains"
SERVERNAME="$DOMAIN www.$DOMAIN"
# Set certbot domain
CERTBOT_DOMAIN="$DOMAIN -d www.$DOMAIN"

# We'll delete default server block beacuse we're building new "default" one
ok "Removing the old default server block"
rm /etc/nginx/sites-enabled/default
else 
ok "Skipping some stuff beacuse we're in local mode"
# In case we have a specific folder name, will set the server name to "FOLDER.DOMAIN"
ok "Setting up server names and certbot domains"
SERVERNAME="$FOLDER.$DOMAIN"
# Set certbot domain
CERTBOT_DOMAIN="$FOLDER.$DOMAIN"
fi

# If we have set the optional parameter subdomain change the server name to "SUBDOMAIN.DOMAIN"
if [ ! -z "$SUBDOMAIN" ]
 then 
    ok "Overriding servernames and cerbot domains because we've set $SUBDOMAIN variable"
    SERVERNAME="$SUBDOMAIN.$DOMAIN"
    CERTBOT_DOMAIN="$SUBDOMAIN.$DOMAIN"
fi

#Check if the file exists, if it does delete it
ok "Check if default nginx block exists, and delete it if it does"
if [ -f $NGINX_AVAILABLE_VHOSTS/default ]; then
    ok "Unlink and remove file default from sites-available and sites-enabled"
    unlink $NGINX_ENABLED_VHOSTS/default
    rm -rf $NGINX_AVAILABLE_VHOSTS/default
fi


#Check if the file exists, if it does delete it
ok "Check if current file name nginx block exists, and delete it if it does"
if [  -f $NGINX_AVAILABLE_VHOSTS/$FOLDER ]; then
    ok "Unlink and remove file ($FOLDER name is used) from sites-available and sites-enabled"
    unlink $NGINX_ENABLED_VHOSTS/$FOLDER
    rm -rf $NGINX_AVAILABLE_VHOSTS/$FOLDER
fi

# Write into the file
ok "Generate new file"
cat > $NGINX_AVAILABLE_VHOSTS/$FOLDER <<EOF
server {
         listen 80;
         listen [::]:80;
 
         # Log files for debugging
         access_log $WEB_DIR/$FOLDER/logs/laravel-access.log;
         error_log $WEB_DIR/$FOLDER/logs/laravel-error.log;
 
         # Webroot directory for Laravel project
         root $WEB_DIR/$FOLDER/public;
         index index.php index.html index.htm;
 
         # Domain dame
         server_name $SERVERNAME;

         # Gzip and gzip setup
         gzip on;
         gzip_vary on;
         gzip_proxied any;
         gzip_types text/css text/plain text/javascript application/javascript application/json application/x-javascript application/xml application/xml+rss application/xhtml+xml application/x-font-ttf application/x-font-opentype application/vnd.ms-fontobject image/svg+xml image/x-icon application/rss+xml application/atom_xml;
         gzip_comp_level 9;
         gzip_http_version 1.0;
         gzip_min_length 50;
         
         # Hide NGINX version
         server_tokens off;

         # Add bunch of headers
         add_header X-XSS-Protection "1; mode=block";
         add_header X-Content-Type-Options nosniff;
         add_header X-Frame-Options SAMEORIGIN;
         add_header Strict-Transport-Security "max-age=15768000" always;

         #ssl_chipers_to_be_inserted

         location / {
                 try_files \$uri \$uri/ /index.php?\$query_string;
         }

         location ~ /\.ht {
                 deny all;
         }

         $PHPMYADMIN_BLOCK

         location ~* \.(jpg|jpeg|png|gif|ico|css|js)$ {
                expires 30d;
                add_header Cache-Control "public, no-transform";

         }
         
         # PHP-FPM Configuration Nginx
         location ~ \.php$ {
                 try_files \$uri =404;
                 fastcgi_split_path_info ^(.+\.php)(/.+)$;
                 fastcgi_pass unix:/run/php/php7.3-fpm.sock;
                 fastcgi_index index.php;
                 fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
                 include fastcgi_params;
         }
 }
EOF

# Create logs directory (we need this in order for nginx restart to pass)
ok "Creating logs folder for our logs"
mkdir -p $WEB_DIR/$FOLDER/logs

# Change the folder permissions.
ok "Setting up the permissions"
chown -R $USER:$WEB_USER $WEB_DIR/$FOLDER

# Enable site by creating symbolic link.
ok "Setting up symlinks"
ln -s $NGINX_AVAILABLE_VHOSTS/$FOLDER $NGINX_ENABLED_VHOSTS/$FOLDER 2> /dev/null
# Restart the Nginx server.
ok "Restarting NGINX"
systemctl restart nginx.service

# Tell the user that subdomain has been created
ok "Subdomain $SERVERNAME is created, folder is  $WEB_DIR/$FOLDER."

# Set up certbot for our CERTBOT_DOMAIN 
# --nginx - Tell certbot we're setting this up for NGINX
# -d - specify domain that we want to get certificate for (you can stack more of these ex. -d xx -d xx -d xx)
# -n - non interactive; we require no input from the user
# --agree-tos - Automatically agree TOS
# -m - Automatically set email address
# --redirect - Set up redirect from HTTP to HTTPS
if [[ $TYPE == "prod" ]]
then
    sudo certbot --nginx -d $CERTBOT_DOMAIN -n --agree-tos -m $EMAIL --redirect
    # After SSL has been set up by certbot, change some lines in our file
    # Comment out ssl options (this is beacuse HTTP2 won't work with these options)
    sed -i 's/include \/etc/#include \/etc/g' $NGINX_AVAILABLE_VHOSTS/$FOLDER
    sed -i 's/#ssl_chipers_to_be_inserted/ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;/g' $NGINX_AVAILABLE_VHOSTS/$FOLDER
else
    ok "Skipping certbot and SSL conf setup"
fi


if [[ $TYPE == "prod" ]]
then
    # Check if our folder name is base or html
    if [[ $FOLDER == "base" || $FOLDER == "html" ]] ;then
    # Add http2 to ipv4 and ipv6
    sed -i 's/listen [::]:443 ssl ipv6only=on;/listen [::]:443 http2 ssl ipv6only=on;/g' $NGINX_AVAILABLE_VHOSTS/$FOLDER
    else 
    # Add http2 to ipv4 and ipv6
    sed -i 's/listen 443 ssl;/listen 443 http2 ssl;/g' $NGINX_AVAILABLE_VHOSTS/$FOLDER
    fi
else
ok "Skip setting up ssl and http2"
#In case of local we need to set up /etc/hosts file
ok "Setting up /etc/hosts"
sudo echo "127.0.0.1 \t $SERVERNAME" >> /etc/hosts
fi

# Tell the user everything has been set up
ok "Finished setting up."
