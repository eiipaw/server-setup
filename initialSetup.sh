#!/bin/bash
#
# Bash script for initial setup of the server
clear

# Functions
ok() { echo -e '\e[32m'$1'\e[m'; } # Green
die() { echo -e '\e[1;31m'$1'\e[m'; exit 1; }


# Check if this script has been run as root
if [ "$EUID" -ne 0 ]
  then 
    die "Script must be running as root."
fi

USAGE="$(basename "$0") -t [-u -e -n]
\n\n
***Initial server setup***
\n\n
What is installed?\n
UFW\n
NGINX\n
PHP7.3\n
nano\n
redis-server \n
NodeJS 8.x LTS\n
npm \n
PHP7.3-fpm\n
Composer \n
mysql-server\n
PhpMyAdmin\n
yarn\n
certbot(for NGINX)
\n\n
where:\n
    -t  Type of installation TYPE=string  VALUES=prod|local DEFAULT=N/A REQUIRED=yes\n
    -u  New user name to add  TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=no\n
    -e  Email for Git TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=no\n
    -n  Name for Git  TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=no"


#Assign arguments to variables
while getopts u:e:n:t:h: option
    do
        case "${option}" in
        u) USER=${OPTARG};;
        e) EMAIL=${OPTARG};;
        n) NAME=${OPTARG};;
        t) TYPE=${OPTARG};;
        h) echo "$USAGE" exit;;
    esac
done 

#Check if all the required variables are set
if [[ -z "$TYPE" ]]
 then
    die "$USAGE"
fi

#Check if the required variables are set properly
if [[ $TYPE != "prod" && $TYPE != "local" ]]
 then
    die "$USAGE"
fi

#Check if variable is NOT null
if [ ! -z "$USER" ]
 then 
# Add user, allow him sudo permissions, copy ssh keys to his directory(that will automatically be created) so he can connect using SSH keys
    adduser $USER
    usermod -aG sudo $USER
    rsync --archive --chown=$USER:$USER ~/.ssh /home/$USER
fi

#
# START SERVER SETUP
#

#Adding ppa in order to install PHP 7.3
ok "Adding ppa in order to instal PHP 7.3"
yes | sudo add-apt-repository ppa:ondrej/php

# Update(get newest versions of available repos) and upgrade server
ok "Running update/upgrade/dist-upgrade"
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade

# Install NGINX that will be our web server
ok "Installing NGINX"
sudo apt-get -y install nginx

if [[ $TYPE == "prod" ]]
then
    # Install UFW (Firewall)
    ok "Installing ufw"
    sudo apt-get -y install ufw
    #Allow OpenSSH and Nginx HTTP
    ok "Opening up the ports on UFW"
    ufw allow OpenSSH
    sudo ufw allow 'Nginx HTTP'
    yes | ufw enable
else
    ok "Skipping installing UFW and setting up ports"
fi

# Install php and bunch of other stuff and dependencies
ok "Installing php and dependencies"
sudo apt -y install php7.3 php7.3-curl php7.3-common php7.3-cli php7.3-mysql php7.3-mbstring php7.3-fpm php7.3-xml php7.3-zip

# Install nano (text editor) and redis-server
ok "Installing nano and redis-server"
sudo apt-get -y install nano
sudo apt-get -y install redis-server

# Install curl and fetch repo for nodejs, install 8.x LTS NodeJS
ok "Installing nodejs and npm"
sudo apt-get -y install curl python-software-properties
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get -y install nodejs

# Start php-fpm process and enable it to start on server restart
ok "Setting up php-fpm"
systemctl start php7.3-fpm
systemctl enable php7.3-fpm

# Install composer, unzip, mysql-server and phpmyadmin
ok "Installing composer, mysql-server and phpmyadmin"
sudo apt-get -y install composer
sudo apt-get -y install unzip 
sudo apt-get -y install mysql-server
sudo apt-get -y install phpmyadmin php-gettext

#Fix permissions on .composer folder
ok "Fixing some permissions"

sudo chown -R $USER:$USER $HOME/.composer
# Install yarn (it's faster than npm in installing)
npm install -g yarn

# Replace text in some files (in order that nginx and phpmyadmin work properly)
ok "Changing some files internally...needed in order to for things to function properly..."
ok "Setting cgi.fix_pathinfo to 0 in php.ini so that our NGINX config works"
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/7.3/fpm/php.ini
ok "Fixing the bug with phpmyadmin count problem when using  php 7.1+ "
sed -i "s/count(\$analyzed_sql_results['select_expr'] == 1)/((count(\$analyzed_sql_results['select_expr']) == 1)/g" /usr/share/phpmyadmin/libraries/sql.lib.php


if [[ $TYPE == "prod" ]]
then
   #Check if the file exists, if it doesn't ask for password and create a new file
    ok "Securing our phpmyadmin with a pass"
    if [ ! -f /etc/nginx/pma_pass ]; then
    # Type in the password for phpmyadmin website auth
    echo "Please enter password for phpmyadmin site authentication:"
    passw="$(openssl passwd)"
    sudo touch /etc/nginx/pma_pass
    echo "${USER}:${passw}" > /etc/nginx/pma_pass
    fi

    # Install certbot for nginx (this is needed for SSL certificates)
    ok "Installing certbot for NGINX in order to obtain SSL certificates"
    yes | sudo add-apt-repository ppa:certbot/certbot
    sudo apt -y install python-certbot-nginx

    # Allow Full Nginx(HTTP and HTTPS) through firewall and remove old rule for just HTTP
    sudo ufw allow 'Nginx Full'
    sudo ufw delete allow 'Nginx HTTP'
else
    ok "Skipping setting up secured phpmyadmin,installing certbot and setting up ports"
fi

# Restart nginx service
ok "Restarting NGINX and PHP 7.3-FPM"
sudo systemctl restart nginx.service
sudo systemctl restart php7.3-fpm.service

#In case EMAIL and NAME variables are set, config git 
if [[ ! -z "$EMAIL" && ! -z "$NAME" ]]
 then 
    git config --global user.email "$EMAIL"
    git config --global user.name "$NAME"
else
 ok "Skipping setting up git user.* because it was not provided"
fi

ok "Everything installed."
