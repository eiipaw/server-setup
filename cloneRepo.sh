#!/bin/bash
#
# Bash script for automatically setting up laravel project from gitlab repository
clear

# Functions
ok() { echo -e '\e[32m'$1'\e[m'; } # Green
die() { echo -e '\e[1;31m'$1'\e[m'; exit 1; }

# Check if this script has been run as root
if [ "$EUID" -ne 0 ]
  then 
    die "Script must be running as root."
fi

USAGE="$(basename "$0") -t -f -r [-d -u -p -w]
\n\n
***Automatic clone and laravel app setup***
\n\n

where:\n
    -t  Type of installation TYPE=string  VALUES=prod|local DEFAULT=N/A REQUIRED=yes\n
    -f  Folder that was created in \$WEB_DIR TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=yes\n
    -r  Repo link to clone TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=yes\n
    -d  Database name  TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=no\n
    -u  Username for database TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=no\n
    -p  Password for database TYPE=string  VALUES=N/A DEFAULT=N/A REQUIRED=no\n
    -w  \$WEB_DIR Directory for application TYPE=string  VALUES=N/A DEFAULT=/var/www REQUIRED=no"

# Variable definitions
ok "Initializing variables"
SSH_KEYS_PATH="/home/$SUDO_USER/.ssh"
BASE_PATH="/var/www"


#Assign arguments to variables
while getopts f:r:d:u:p:t:w:h: option
    do
        case "${option}"
            in
                f) FOLDER=${OPTARG};;
                r) REPO=${OPTARG};;
                d) DB_DATABASE=${OPTARG};;
                u) DB_USERNAME=${OPTARG};;
                p) DB_PASSWORD=${OPTARG};;
                t) TYPE=${OPTARG};;
                w) BASE_PATH=${OPTARG};;
                h) echo "$USAGE" exit;;
        esac
done 

# Variable definitions that depend on the input
FULL_PATH="$BASE_PATH/$FOLDER"

#Check if all the required variables are set
if [[ -z "$FOLDER" || -z "$REPO" || -z "$TYPE" ]]
 then
    die "$USAGE"
fi

#Check if the required variables are set properly
if [[ $TYPE != "prod" && $TYPE != "local" ]]
 then
    die "$USAGE"
fi


# Check if user has set up SSH keys by checking if .pub file exists
if [ ! -f "$SSH_KEYS_PATH/id_rsa.pub" ]
 then
    ok "Can't find user ssh key, trying with root ssh key"
    SSH_KEYS_PATH="/root/.ssh"
    if [ ! -f "$SSH_KEYS_PATH/id_rsa.pub" ]
    then
        die "We can't find ssh keys (.pub file) \n Please check docs https://docs.gitlab.com/ee/ssh/ on how to set up your ssh keys"    
    fi
fi


#
# START PROJECT SETUP
# 

# Go to base path of all projects
ok "Cd-ing to base path"
cd $BASE_PATH

# Remove the folder (We know that there will be $FOLDER beacuse it is created when subdomainGenerator.sh was started)
ok "Removing created folder with only logs inside"
sudo rm -rf $FOLDER 

# Clone repo with the name $FOLDER (This is why we previously removed $FOLDER)
ok "Cloning the repo with automatic confirmation for fingerprint"
yes | GIT_SSH_COMMAND="/usr/bin/ssh -i $SSH_KEYS_PATH/id_rsa" git clone $REPO $FOLDER
RETVAL=$?
    if [ ! $RETVAL -eq 0 ]; then
        # We couldn't clone the repo
        die "Problem while cloning the repo. Check your repo and ssh keys."
    fi


#Give the right ownership to the folder
ok "Giving the right ownership to folder"
chown -R "${SUDO_USER}:${SUDO_USER}" $FOLDER

# Go into newly cloned project folder
ok "Cd-ing into the project"
cd $FOLDER

# Install all files using composer
ok "Composer install, yarn install"
composer install

# Install cross-env beacuse there are cases when it's missing
# --save - this will save it to current projects' package.json
yarn add cross-env --save

# Install node_modules using yarn (faster than npm)
yarn install

ok "Running yarn run production or development based on TYPE inputed"
if [[ $TYPE == "prod" ]]
then
  # Build production environment (same as npm run prod)
    yarn run production
else
    # Build development environment (same as npm run dev)
    yarn run development
fi

# Set folder permissions
# -R - recursively
ok "Setting up 777 on storage recursively"
chmod -R 777 storage


# Check if our folder name is base or html
if [[ $FOLDER == "base" || $FOLDER == "html" ]] && [ $TYPE == "prod" ] ;then
    # Create smylink for phpmyadmin to /pmasecure
    # NOTE: We need to be root for this to work
    ok "Creating symlink for phpmyadmin"
    sudo ln -s /usr/share/phpmyadmin $FULL_PATH/public/pmasecure
fi

# Copy .env.example to .env
ok "Copying .env.example and setting up key and storage link"
cp .env.example .env

# Set up key
php artisan key:gen

# Set up symlink for storage
php artisan storage:link


#Check if username is not null
if [[ ! -z "$DB_USERNAME" ]]
 then
    #Check if password is null
    if [[ -z "$DB_PASSWORD" ]]
    then
        #If username was set but password was not, assign default value to $DB_PASSWORD
        DB_PASSWORD=""
    fi   
fi    

#Check if all additional variables are set ($DB_PASSWORD will always be set)
if [[ -z "$DB_DATABASE" || -z "$DB_USERNAME" ]]
 then
    # If they aren't then open .env file and let user manually set the database info
    ok "You need to manually create database and set password for mysql"
    sudo nano .env
    ok "Don't forget to create the database and alter the user password"
    
else
    # If they are, automatically change database info based on given variables
    ok "Setting up database, username and password in .env file"
    sed -i "s/DB_DATABASE=homestead/DB_DATABASE=$DB_DATABASE/g" .env
    sed -i "s/DB_USERNAME=homestead/DB_USERNAME=$DB_USERNAME/g" .env
    sed -i "s/DB_PASSWORD=secret/DB_PASSWORD=$DB_PASSWORD/g" .env

    # Check if we can automatically run mysql commands
    ok "Check if we have mysql permissions"
    sudo mysql --execute=""
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
        # Automatically create database in mysql
        ok "Automatically creating database in mysql"
        sudo mysql --execute="CREATE DATABASE $DB_DATABASE;"
    else
        ok "We don't have the permissions, you'll have to create the database yourself"
    fi
fi

# Clear config and cache files
ok "Clearing up the config and cache"
php artisan co:cl && php artisan ca:cl

# Create logs folder and create log files inside (We need this for nginx to work)
ok "Creating our log folder and log files"
mkdir logs
touch logs/laravel-error.log
touch logs/laravel-access.log


if [[ $RETVAL -eq 0 ]]
then
    # Run database migrations and seeders
    ok "Migrating and seeding the database"
    php artisan migrate:fresh --seed
    
else
    ok "You need to run MIGRATIONS AND SEEDERS yourself!"
fi



if [[ $TYPE == "prod" ]]
then
    # Change some variables in .env file (for production)
    ok "Changing more variables in .env file for production"
    sed -i 's/APP_ENV=local/APP_ENV=production/g' .env
    sed -i 's/APP_DEBUG=true/APP_DEBUG=false/g' .env
else
    ok "Skiping production changes on .env"
fi

#Give the right ownership to the project folder
ok "Giving the right ownership to project folder"
sudo chown -R "${SUDO_USER}:${SUDO_USER}" $FULL_PATH

# Restart NGINX
ok "One last NGINX restart"
sudo systemctl restart nginx.service


# Notify user that all is set
ok "All set, you're ready to roll."