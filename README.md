# Server setup (LEMP stack) and project setup

These scripts are used to:

1. Setup the server
2. Setup the subdomains
3. Clone and setup the project

## What will be installed

1. UFW
2. NGINX
3. PHP7.3
4. nano
5. redis-server 
6. NodeJS 8.x LTS
7. npm 
8. PHP7.3-fpm
9. Composer 
10. mysql-server
11. PhpMyAdmin
12. yarn
13. certbot(for NGINX)



### You need to set the .sh file to be executable

chmod +x *.sh 

### Running the scripts

sudo bash initialSetup.sh ...
 
## NOTES

#### initialSetup.sh

 Certbot is automatically set to agree tos, redirect http->https and the mail is hardcoded to helpereiden@gmail.com
  
#### subdomainGenerator.sh
 
 "Base" domain is hardcoded to be setup as folder base or html
 
 PhpMyAdmin access url is hardcoded to /pmasecure

